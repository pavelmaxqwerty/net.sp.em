package main.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/home")
public class HomeController {

    @RequestMapping(method = {RequestMethod.GET})
    public String indexAction(ModelMap model) {
        model.addAttribute("test", "test value");
        return "home";
    }
}
