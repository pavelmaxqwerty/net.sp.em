package main.model.entity;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;


@MappedSuperclass
public abstract class AbstactMessage extends AbstactEntity {
    @Size(min = 1, max = 255)
    @NotNull
    @Column(nullable = false, length = 255)
    String content;
    @Temporal(TemporalType.TIMESTAMP)
    @CreationTimestamp
    Date createDate;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("AbstactMessage{");
        sb.append("content='").append(content).append('\'');
        sb.append(", createDate=").append(createDate);
        sb.append(", id=").append(id);
        sb.append('}');
        return sb.toString();
    }
}
