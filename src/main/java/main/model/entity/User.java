package main.model.entity;

import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
public class User extends AbstactEntity {
    @Size(min = 1, max = 50)
    @NotNull
    @Column(nullable = false, length = 50, unique = true)
    String userName;
    @NotNull
    @Column(nullable = false)
    String userPassword;
    @Size(min = 1, max = 50)
    @Column(nullable = false, length = 50)
    String firstName;
    @Size(min = 1, max = 50)
    @Column(nullable = false, length = 50)
    String lastName;
    @Column(nullable = true)
    String middleNames;
    @Column(nullable = true)
    Short age;
    @Email
    @Column(nullable = true)
    String email;
    @OneToMany(targetEntity = News.class, mappedBy = "id", cascade = CascadeType.REMOVE)
    Set<News> messages;
    @OneToMany(targetEntity = Comment.class, mappedBy = "id", cascade = CascadeType.REMOVE)
    Set<Comment> comments;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleNames() {
        return middleNames;
    }

    public void setMiddleNames(String middleNames) {
        this.middleNames = middleNames;
    }

    public Short getAge() {
        return age;
    }

    public void setAge(Short age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<News> getMessages() {
        return messages;
    }

    public void setMessages(Set<News> messages) {
        this.messages = messages;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;

        User user = (User) o;

        if (userName != null ? !userName.equals(user.userName) : user.userName != null) return false;
        if (userPassword != null ? !userPassword.equals(user.userPassword) : user.userPassword != null) return false;
        if (firstName != null ? !firstName.equals(user.firstName) : user.firstName != null) return false;
        if (lastName != null ? !lastName.equals(user.lastName) : user.lastName != null) return false;
        if (middleNames != null ? !middleNames.equals(user.middleNames) : user.middleNames != null) return false;
        if (age != null ? !age.equals(user.age) : user.age != null) return false;
        if (email != null ? !email.equals(user.email) : user.email != null) return false;
        if (messages != null ? !messages.equals(user.messages) : user.messages != null) return false;
        return comments != null ? comments.equals(user.comments) : user.comments == null;
    }

    @Override
    public int hashCode() {
        int result = userName != null ? userName.hashCode() : 0;
        result = 31 * result + (userPassword != null ? userPassword.hashCode() : 0);
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (middleNames != null ? middleNames.hashCode() : 0);
        result = 31 * result + (age != null ? age.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (messages != null ? messages.hashCode() : 0);
        result = 31 * result + (comments != null ? comments.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("User{");
        sb.append("userName='").append(userName).append('\'');
        sb.append(", userPassword='").append(userPassword).append('\'');
        sb.append(", firstName='").append(firstName).append('\'');
        sb.append(", lastName='").append(lastName).append('\'');
        sb.append(", middleNames='").append(middleNames).append('\'');
        sb.append(", age=").append(age);
        sb.append(", email='").append(email).append('\'');
        sb.append(", messages=").append(messages);
        sb.append(", comments=").append(comments);
        sb.append(", id=").append(id);
        sb.append('}');
        return sb.toString();
    }
}
